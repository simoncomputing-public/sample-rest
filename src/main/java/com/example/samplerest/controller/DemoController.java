package com.example.samplerest.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.samplerest.model.Person;

@RestController
public class DemoController {
	
	// Example without parameters
	// http://localhost:8080/hello
	@RequestMapping( value="/hello", method=RequestMethod.GET)
	public String hello() {
		return "Hello!";
	}
	
	// Example where parameter is part of the request path
	// http://localhost:8080/get-person/001
	@RequestMapping( value="/get-person/{id}", method=RequestMethod.GET)
	public Person getPerson( @PathVariable( "id" ) String id ) {
		return new Person( id, "Superman" );
	}
	
	// Example where parameters are sent as parameters in URL
	// http://localhost:8080/create-person?id=001&name=WonderWoman
	@RequestMapping( value="/create-person", method=RequestMethod.POST)
	public Person createPerson( @RequestParam( "id" ) String id,
							 @RequestParam( "name" ) String name 	) {
		return new Person( id, name );
	}
	
	// Example for Form submission
	// curl -X POST 
	//      --header 'Content-Type: application/json' 
	//      --header 'Accept: application/json' 
	//      -d '{ "id": "003", "name": "Batman"}' 
	//      'http://localhost:8080/echo-person'
	@RequestMapping( value="/echo-person", method=RequestMethod.POST)
 	public Person  echoPerson( @RequestBody Person person ) {
		return person;
	}
	
}

