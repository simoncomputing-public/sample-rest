package com.example.samplerest.model;

public class Person {
	private String id;
	private String name;
	
	// A default constructor is needed to allow
	// a Person object to be received as a JSON
	// parameter
	public Person() {}
	
	public Person( String id, String name ) {
		this.id = id;
		this.name = name;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}
